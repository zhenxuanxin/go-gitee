# Commit

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Sha** | **string** |  | [optional] [default to null]
**Author** | **string** |  | [optional] [default to null]
**Committer** | **string** |  | [optional] [default to null]
**Message** | **string** |  | [optional] [default to null]
**Tree** | **string** |  | [optional] [default to null]
**Parents** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


# EnterpriseMembersBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AccessToken** | **string** | 用户授权码 | [optional] [default to null]
**Username** | **string** | 需要邀请的用户名(username/login)，username,email至少填写一个 | [optional] [default to null]
**Email** | **string** | 要添加邮箱地址，若该邮箱未注册则自动创建帐号。username,email至少填写一个 | [optional] [default to null]
**Role** | **string** | 企业角色：member &#x3D;&gt; 普通成员, outsourced &#x3D;&gt; 外包成员, admin &#x3D;&gt; 管理员 | [optional] [default to ROLE.MEMBER]
**Name** | **string** | 企业成员真实姓名（备注） | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


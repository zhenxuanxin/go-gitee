# EnterpriseWeekReportBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AccessToken** | **string** | 用户授权码 | [optional] [default to null]
**Year** | **int32** | 周报所属年 | [default to null]
**Content** | **string** | 周报内容 | [default to null]
**WeekIndex** | **int32** | 周报所属周 | [default to null]
**Date** | **string** | 周报日期(格式：2019-03-25) | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


# Hook

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **int32** |  | [optional] [default to null]
**Url** | **string** |  | [optional] [default to null]
**CreatedAt** | **string** |  | [optional] [default to null]
**Password** | **string** |  | [optional] [default to null]
**ProjectId** | **int32** |  | [optional] [default to null]
**Result** | **string** |  | [optional] [default to null]
**ResultCode** | **int32** |  | [optional] [default to null]
**PushEvents** | **bool** |  | [optional] [default to null]
**TagPushEvents** | **bool** |  | [optional] [default to null]
**IssuesEvents** | **bool** |  | [optional] [default to null]
**NoteEvents** | **bool** |  | [optional] [default to null]
**MergeRequestsEvents** | **bool** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


# IssueState

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **int32** | 任务状态 ID | [optional] [default to null]
**Title** | **string** | 任务状态的名称 | [optional] [default to null]
**Color** | **string** | 任务状态的颜色 | [optional] [default to null]
**Icon** | **string** | 任务状态的 Icon | [optional] [default to null]
**Command** | **string** | 任务状态的 指令 | [optional] [default to null]
**Serial** | **string** | 任务状态的 排序 | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


# IssueType

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **int32** | 任务类型 ID | [optional] [default to null]
**Title** | **string** | 任务类型的名称 | [optional] [default to null]
**Template** | **string** | 任务类型模板 | [optional] [default to null]
**Ident** | **string** | 唯一标识符 | [optional] [default to null]
**Color** | **string** | 颜色 | [optional] [default to null]
**IsSystem** | **bool** | 是否系统默认类型 | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


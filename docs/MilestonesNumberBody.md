# MilestonesNumberBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AccessToken** | **string** | 用户授权码 | [optional] [default to null]
**Title** | **string** | 里程碑标题 | [default to null]
**State** | **string** | 里程碑状态: open, closed, all。默认: open | [optional] [default to STATE.OPEN]
**Description** | **string** | 里程碑具体描述 | [optional] [default to null]
**DueOn** | **string** | 里程碑的截止日期 | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


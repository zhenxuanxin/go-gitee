# NamespaceMini

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **int32** | namespace id | [optional] [default to null]
**Type_** | **string** | namespace 类型，企业：Enterprise，组织：Group，用户：null | [optional] [default to null]
**Name** | **string** | namespace 名称 | [optional] [default to null]
**Path** | **string** | namespace 路径 | [optional] [default to null]
**HtmlUrl** | **string** | namespace 地址 | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


# OrgsOrgBody1

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AccessToken** | **string** | 用户授权码 | [optional] [default to null]
**Email** | **string** | 组织公开的邮箱地址 | [optional] [default to null]
**Location** | **string** | 组织所在地 | [optional] [default to null]
**Name** | **string** | 组织名称 | [optional] [default to null]
**Description** | **string** | 组织简介 | [optional] [default to null]
**HtmlUrl** | **string** | 组织站点 | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


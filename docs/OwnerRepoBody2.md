# OwnerRepoBody2

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AccessToken** | **string** | 用户授权码 | [optional] [default to null]
**WatchType** | **string** | watch策略, watching: 关注所有动态, ignoring: 关注但不提醒动态 | [default to WATCH_TYPE.WATCHING]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


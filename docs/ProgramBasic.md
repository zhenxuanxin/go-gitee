# ProgramBasic

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **int32** | 项目id | [optional] [default to null]
**Name** | **string** | 项目名称 | [optional] [default to null]
**Description** | **string** | 项目描述 | [optional] [default to null]
**Assignee** | [***UserBasic**](UserBasic.md) |  | [optional] [default to null]
**Author** | [***UserBasic**](UserBasic.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


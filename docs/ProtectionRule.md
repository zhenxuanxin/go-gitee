# ProtectionRule

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | [optional] [default to null]
**ProjectId** | **string** |  | [optional] [default to null]
**Wildcard** | **string** |  | [optional] [default to null]
**Pushers** | **[]string** |  | [optional] [default to null]
**Mergers** | **[]string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


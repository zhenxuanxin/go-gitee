# PullRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | [optional] [default to null]
**Url** | **string** |  | [optional] [default to null]
**HtmlUrl** | **string** |  | [optional] [default to null]
**DiffUrl** | **string** |  | [optional] [default to null]
**PatchUrl** | **string** |  | [optional] [default to null]
**IssueUrl** | **string** |  | [optional] [default to null]
**CommitsUrl** | **string** |  | [optional] [default to null]
**ReviewCommentsUrl** | **string** |  | [optional] [default to null]
**ReviewCommentUrl** | **string** |  | [optional] [default to null]
**CommentsUrl** | **string** |  | [optional] [default to null]
**Number** | **string** |  | [optional] [default to null]
**State** | **string** |  | [optional] [default to null]
**Title** | **string** |  | [optional] [default to null]
**Body** | **string** |  | [optional] [default to null]
**BodyHtml** | **string** |  | [optional] [default to null]
**AssigneesNumber** | **string** |  | [optional] [default to null]
**TestersNumber** | **string** |  | [optional] [default to null]
**Assignees** | **[]string** |  | [optional] [default to null]
**Testers** | **[]string** |  | [optional] [default to null]
**ApiReviewersNumber** | **string** |  | [optional] [default to null]
**ApiReviewers** | **[]string** |  | [optional] [default to null]
**Milestone** | [***Milestone**](Milestone.md) |  | [optional] [default to null]
**Labels** | [***Label**](Label.md) |  | [optional] [default to null]
**Locked** | **string** |  | [optional] [default to null]
**CreatedAt** | **string** |  | [optional] [default to null]
**UpdatedAt** | **string** |  | [optional] [default to null]
**ClosedAt** | **string** |  | [optional] [default to null]
**MergedAt** | **string** |  | [optional] [default to null]
**Mergeable** | **string** |  | [optional] [default to null]
**CanMergeCheck** | **string** |  | [optional] [default to null]
**Head** | **string** |  | [optional] [default to null]
**Base** | **string** |  | [optional] [default to null]
**Links** | **string** |  | [optional] [default to null]
**User** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


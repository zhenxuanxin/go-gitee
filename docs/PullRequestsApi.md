# {{classname}}

All URIs are relative to *https://gitee.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**DeleteV5ReposOwnerRepoPullsCommentsId**](PullRequestsApi.md#DeleteV5ReposOwnerRepoPullsCommentsId) | **Delete** /v5/repos/{owner}/{repo}/pulls/comments/{id} | 删除评论
[**DeleteV5ReposOwnerRepoPullsNumberAssignees**](PullRequestsApi.md#DeleteV5ReposOwnerRepoPullsNumberAssignees) | **Delete** /v5/repos/{owner}/{repo}/pulls/{number}/assignees | 取消用户审查 Pull Request
[**DeleteV5ReposOwnerRepoPullsNumberLabelsName**](PullRequestsApi.md#DeleteV5ReposOwnerRepoPullsNumberLabelsName) | **Delete** /v5/repos/{owner}/{repo}/pulls/{number}/labels/{name} | 删除 Pull Request 标签
[**DeleteV5ReposOwnerRepoPullsNumberTesters**](PullRequestsApi.md#DeleteV5ReposOwnerRepoPullsNumberTesters) | **Delete** /v5/repos/{owner}/{repo}/pulls/{number}/testers | 取消用户测试 Pull Request
[**GetV5ReposOwnerRepoPulls**](PullRequestsApi.md#GetV5ReposOwnerRepoPulls) | **Get** /v5/repos/{owner}/{repo}/pulls | 获取Pull Request列表
[**GetV5ReposOwnerRepoPullsCommentsId**](PullRequestsApi.md#GetV5ReposOwnerRepoPullsCommentsId) | **Get** /v5/repos/{owner}/{repo}/pulls/comments/{id} | 获取Pull Request的某个评论
[**GetV5ReposOwnerRepoPullsNumber**](PullRequestsApi.md#GetV5ReposOwnerRepoPullsNumber) | **Get** /v5/repos/{owner}/{repo}/pulls/{number} | 获取单个Pull Request
[**GetV5ReposOwnerRepoPullsNumberComments**](PullRequestsApi.md#GetV5ReposOwnerRepoPullsNumberComments) | **Get** /v5/repos/{owner}/{repo}/pulls/{number}/comments | 获取某个Pull Request的所有评论
[**GetV5ReposOwnerRepoPullsNumberCommits**](PullRequestsApi.md#GetV5ReposOwnerRepoPullsNumberCommits) | **Get** /v5/repos/{owner}/{repo}/pulls/{number}/commits | 获取某Pull Request的所有Commit信息。最多显示250条Commit
[**GetV5ReposOwnerRepoPullsNumberFiles**](PullRequestsApi.md#GetV5ReposOwnerRepoPullsNumberFiles) | **Get** /v5/repos/{owner}/{repo}/pulls/{number}/files | Pull Request Commit文件列表。最多显示300条diff
[**GetV5ReposOwnerRepoPullsNumberIssues**](PullRequestsApi.md#GetV5ReposOwnerRepoPullsNumberIssues) | **Get** /v5/repos/{owner}/{repo}/pulls/{number}/issues | 获取 Pull Request 关联的 issues
[**GetV5ReposOwnerRepoPullsNumberLabels**](PullRequestsApi.md#GetV5ReposOwnerRepoPullsNumberLabels) | **Get** /v5/repos/{owner}/{repo}/pulls/{number}/labels | 获取某个 Pull Request 的所有标签
[**GetV5ReposOwnerRepoPullsNumberMerge**](PullRequestsApi.md#GetV5ReposOwnerRepoPullsNumberMerge) | **Get** /v5/repos/{owner}/{repo}/pulls/{number}/merge | 判断Pull Request是否已经合并
[**GetV5ReposOwnerRepoPullsNumberOperateLogs**](PullRequestsApi.md#GetV5ReposOwnerRepoPullsNumberOperateLogs) | **Get** /v5/repos/{owner}/{repo}/pulls/{number}/operate_logs | 获取某个Pull Request的操作日志
[**PatchV5ReposOwnerRepoPullsCommentsId**](PullRequestsApi.md#PatchV5ReposOwnerRepoPullsCommentsId) | **Patch** /v5/repos/{owner}/{repo}/pulls/comments/{id} | 编辑评论
[**PatchV5ReposOwnerRepoPullsNumber**](PullRequestsApi.md#PatchV5ReposOwnerRepoPullsNumber) | **Patch** /v5/repos/{owner}/{repo}/pulls/{number} | 更新Pull Request信息
[**PatchV5ReposOwnerRepoPullsNumberAssignees**](PullRequestsApi.md#PatchV5ReposOwnerRepoPullsNumberAssignees) | **Patch** /v5/repos/{owner}/{repo}/pulls/{number}/assignees | 重置 Pull Request 审查 的状态
[**PatchV5ReposOwnerRepoPullsNumberTesters**](PullRequestsApi.md#PatchV5ReposOwnerRepoPullsNumberTesters) | **Patch** /v5/repos/{owner}/{repo}/pulls/{number}/testers | 重置 Pull Request 测试 的状态
[**PostV5ReposOwnerRepoPulls**](PullRequestsApi.md#PostV5ReposOwnerRepoPulls) | **Post** /v5/repos/{owner}/{repo}/pulls | 创建Pull Request
[**PostV5ReposOwnerRepoPullsNumberAssignees**](PullRequestsApi.md#PostV5ReposOwnerRepoPullsNumberAssignees) | **Post** /v5/repos/{owner}/{repo}/pulls/{number}/assignees | 指派用户审查 Pull Request
[**PostV5ReposOwnerRepoPullsNumberComments**](PullRequestsApi.md#PostV5ReposOwnerRepoPullsNumberComments) | **Post** /v5/repos/{owner}/{repo}/pulls/{number}/comments | 提交Pull Request评论
[**PostV5ReposOwnerRepoPullsNumberLabels**](PullRequestsApi.md#PostV5ReposOwnerRepoPullsNumberLabels) | **Post** /v5/repos/{owner}/{repo}/pulls/{number}/labels | 创建 Pull Request 标签
[**PostV5ReposOwnerRepoPullsNumberReview**](PullRequestsApi.md#PostV5ReposOwnerRepoPullsNumberReview) | **Post** /v5/repos/{owner}/{repo}/pulls/{number}/review | 处理 Pull Request 审查
[**PostV5ReposOwnerRepoPullsNumberTest**](PullRequestsApi.md#PostV5ReposOwnerRepoPullsNumberTest) | **Post** /v5/repos/{owner}/{repo}/pulls/{number}/test | 处理 Pull Request 测试
[**PostV5ReposOwnerRepoPullsNumberTesters**](PullRequestsApi.md#PostV5ReposOwnerRepoPullsNumberTesters) | **Post** /v5/repos/{owner}/{repo}/pulls/{number}/testers | 指派用户测试 Pull Request
[**PutV5ReposOwnerRepoPullsNumberLabels**](PullRequestsApi.md#PutV5ReposOwnerRepoPullsNumberLabels) | **Put** /v5/repos/{owner}/{repo}/pulls/{number}/labels | 替换 Pull Request 所有标签
[**PutV5ReposOwnerRepoPullsNumberMerge**](PullRequestsApi.md#PutV5ReposOwnerRepoPullsNumberMerge) | **Put** /v5/repos/{owner}/{repo}/pulls/{number}/merge | 合并Pull Request

# **DeleteV5ReposOwnerRepoPullsCommentsId**
> DeleteV5ReposOwnerRepoPullsCommentsId(ctx, owner, repo, id, optional)
删除评论

删除评论

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| 仓库所属空间地址(企业、组织或个人的地址path) | 
  **repo** | **string**| 仓库路径(path) | 
  **id** | **int32**| 评论的ID | 
 **optional** | ***PullRequestsApiDeleteV5ReposOwnerRepoPullsCommentsIdOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a PullRequestsApiDeleteV5ReposOwnerRepoPullsCommentsIdOpts struct
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **accessToken** | **optional.String**| 用户授权码 | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **DeleteV5ReposOwnerRepoPullsNumberAssignees**
> PullRequest DeleteV5ReposOwnerRepoPullsNumberAssignees(ctx, owner, repo, number, assignees, optional)
取消用户审查 Pull Request

取消用户审查 Pull Request

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| 仓库所属空间地址(企业、组织或个人的地址path) | 
  **repo** | **string**| 仓库路径(path) | 
  **number** | **int32**| 第几个PR，即本仓库PR的序数 | 
  **assignees** | **string**| 用户的个人空间地址, 以 , 分隔 | 
 **optional** | ***PullRequestsApiDeleteV5ReposOwnerRepoPullsNumberAssigneesOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a PullRequestsApiDeleteV5ReposOwnerRepoPullsNumberAssigneesOpts struct
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------




 **accessToken** | **optional.String**| 用户授权码 | 

### Return type

[**PullRequest**](PullRequest.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **DeleteV5ReposOwnerRepoPullsNumberLabelsName**
> DeleteV5ReposOwnerRepoPullsNumberLabelsName(ctx, owner, repo, number, name, optional)
删除 Pull Request 标签

删除 Pull Request 标签

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| 仓库所属空间地址(企业、组织或个人的地址path) | 
  **repo** | **string**| 仓库路径(path) | 
  **number** | **int32**| 第几个PR，即本仓库PR的序数 | 
  **name** | **string**| 标签名称(批量删除用英文逗号分隔，如: bug,feature) | 
 **optional** | ***PullRequestsApiDeleteV5ReposOwnerRepoPullsNumberLabelsNameOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a PullRequestsApiDeleteV5ReposOwnerRepoPullsNumberLabelsNameOpts struct
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------




 **accessToken** | **optional.String**| 用户授权码 | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **DeleteV5ReposOwnerRepoPullsNumberTesters**
> PullRequest DeleteV5ReposOwnerRepoPullsNumberTesters(ctx, owner, repo, number, testers, optional)
取消用户测试 Pull Request

取消用户测试 Pull Request

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| 仓库所属空间地址(企业、组织或个人的地址path) | 
  **repo** | **string**| 仓库路径(path) | 
  **number** | **int32**| 第几个PR，即本仓库PR的序数 | 
  **testers** | **string**| 用户的个人空间地址, 以 , 分隔 | 
 **optional** | ***PullRequestsApiDeleteV5ReposOwnerRepoPullsNumberTestersOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a PullRequestsApiDeleteV5ReposOwnerRepoPullsNumberTestersOpts struct
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------




 **accessToken** | **optional.String**| 用户授权码 | 

### Return type

[**PullRequest**](PullRequest.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5ReposOwnerRepoPulls**
> []PullRequest GetV5ReposOwnerRepoPulls(ctx, owner, repo, optional)
获取Pull Request列表

获取Pull Request列表

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| 仓库所属空间地址(企业、组织或个人的地址path) | 
  **repo** | **string**| 仓库路径(path) | 
 **optional** | ***PullRequestsApiGetV5ReposOwnerRepoPullsOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a PullRequestsApiGetV5ReposOwnerRepoPullsOpts struct
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **accessToken** | **optional.String**| 用户授权码 | 
 **state** | **optional.String**| 可选。Pull Request 状态 | [default to open]
 **head** | **optional.String**| 可选。Pull Request 提交的源分支。格式：branch 或者：username:branch | 
 **base** | **optional.String**| 可选。Pull Request 提交目标分支的名称。 | 
 **sort** | **optional.String**| 可选。排序字段，默认按创建时间 | [default to created]
 **since** | **optional.String**| 可选。起始的更新时间，要求时间格式为 ISO 8601 | 
 **direction** | **optional.String**| 可选。升序/降序 | [default to desc]
 **milestoneNumber** | **optional.Int32**| 可选。里程碑序号(id) | 
 **labels** | **optional.String**| 用逗号分开的标签。如: bug,performance | 
 **page** | **optional.Int32**| 当前的页码 | [default to 1]
 **perPage** | **optional.Int32**| 每页的数量，最大为 100 | [default to 20]

### Return type

[**[]PullRequest**](PullRequest.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5ReposOwnerRepoPullsCommentsId**
> PullRequestComments GetV5ReposOwnerRepoPullsCommentsId(ctx, owner, repo, id, optional)
获取Pull Request的某个评论

获取Pull Request的某个评论

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| 仓库所属空间地址(企业、组织或个人的地址path) | 
  **repo** | **string**| 仓库路径(path) | 
  **id** | **int32**|  | 
 **optional** | ***PullRequestsApiGetV5ReposOwnerRepoPullsCommentsIdOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a PullRequestsApiGetV5ReposOwnerRepoPullsCommentsIdOpts struct
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **accessToken** | **optional.String**| 用户授权码 | 

### Return type

[**PullRequestComments**](PullRequestComments.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5ReposOwnerRepoPullsNumber**
> PullRequest GetV5ReposOwnerRepoPullsNumber(ctx, owner, repo, number, optional)
获取单个Pull Request

获取单个Pull Request

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| 仓库所属空间地址(企业、组织或个人的地址path) | 
  **repo** | **string**| 仓库路径(path) | 
  **number** | **int32**| 第几个PR，即本仓库PR的序数 | 
 **optional** | ***PullRequestsApiGetV5ReposOwnerRepoPullsNumberOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a PullRequestsApiGetV5ReposOwnerRepoPullsNumberOpts struct
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **accessToken** | **optional.String**| 用户授权码 | 

### Return type

[**PullRequest**](PullRequest.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5ReposOwnerRepoPullsNumberComments**
> []PullRequestComments GetV5ReposOwnerRepoPullsNumberComments(ctx, owner, repo, number, optional)
获取某个Pull Request的所有评论

获取某个Pull Request的所有评论

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| 仓库所属空间地址(企业、组织或个人的地址path) | 
  **repo** | **string**| 仓库路径(path) | 
  **number** | **int32**| 第几个PR，即本仓库PR的序数 | 
 **optional** | ***PullRequestsApiGetV5ReposOwnerRepoPullsNumberCommentsOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a PullRequestsApiGetV5ReposOwnerRepoPullsNumberCommentsOpts struct
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **accessToken** | **optional.String**| 用户授权码 | 
 **page** | **optional.Int32**| 当前的页码 | [default to 1]
 **perPage** | **optional.Int32**| 每页的数量，最大为 100 | [default to 20]
 **direction** | **optional.String**| 可选。升序/降序 | 

### Return type

[**[]PullRequestComments**](PullRequestComments.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5ReposOwnerRepoPullsNumberCommits**
> []PullRequestCommits GetV5ReposOwnerRepoPullsNumberCommits(ctx, owner, repo, number, optional)
获取某Pull Request的所有Commit信息。最多显示250条Commit

获取某Pull Request的所有Commit信息。最多显示250条Commit

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| 仓库所属空间地址(企业、组织或个人的地址path) | 
  **repo** | **string**| 仓库路径(path) | 
  **number** | **int32**| 第几个PR，即本仓库PR的序数 | 
 **optional** | ***PullRequestsApiGetV5ReposOwnerRepoPullsNumberCommitsOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a PullRequestsApiGetV5ReposOwnerRepoPullsNumberCommitsOpts struct
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **accessToken** | **optional.String**| 用户授权码 | 

### Return type

[**[]PullRequestCommits**](PullRequestCommits.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5ReposOwnerRepoPullsNumberFiles**
> []PullRequestFiles GetV5ReposOwnerRepoPullsNumberFiles(ctx, owner, repo, number, optional)
Pull Request Commit文件列表。最多显示300条diff

Pull Request Commit文件列表。最多显示300条diff

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| 仓库所属空间地址(企业、组织或个人的地址path) | 
  **repo** | **string**| 仓库路径(path) | 
  **number** | **int32**| 第几个PR，即本仓库PR的序数 | 
 **optional** | ***PullRequestsApiGetV5ReposOwnerRepoPullsNumberFilesOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a PullRequestsApiGetV5ReposOwnerRepoPullsNumberFilesOpts struct
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **accessToken** | **optional.String**| 用户授权码 | 

### Return type

[**[]PullRequestFiles**](PullRequestFiles.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5ReposOwnerRepoPullsNumberIssues**
> []Issue GetV5ReposOwnerRepoPullsNumberIssues(ctx, owner, repo, number, optional)
获取 Pull Request 关联的 issues

获取 Pull Request 关联的 issues

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| 仓库所属空间地址(企业、组织或个人的地址path) | 
  **repo** | **string**| 仓库路径(path) | 
  **number** | **int32**|  | 
 **optional** | ***PullRequestsApiGetV5ReposOwnerRepoPullsNumberIssuesOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a PullRequestsApiGetV5ReposOwnerRepoPullsNumberIssuesOpts struct
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **accessToken** | **optional.String**| 用户授权码 | 
 **page** | **optional.Int32**| 当前的页码 | [default to 1]
 **perPage** | **optional.Int32**| 每页的数量，最大为 100 | [default to 20]

### Return type

[**[]Issue**](Issue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5ReposOwnerRepoPullsNumberLabels**
> []Label GetV5ReposOwnerRepoPullsNumberLabels(ctx, owner, repo, number, optional)
获取某个 Pull Request 的所有标签

获取某个 Pull Request 的所有标签

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| 仓库所属空间地址(企业、组织或个人的地址path) | 
  **repo** | **string**| 仓库路径(path) | 
  **number** | **int32**| 第几个PR，即本仓库PR的序数 | 
 **optional** | ***PullRequestsApiGetV5ReposOwnerRepoPullsNumberLabelsOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a PullRequestsApiGetV5ReposOwnerRepoPullsNumberLabelsOpts struct
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **accessToken** | **optional.String**| 用户授权码 | 
 **page** | **optional.Int32**| 当前的页码 | [default to 1]
 **perPage** | **optional.Int32**| 每页的数量，最大为 100 | [default to 20]

### Return type

[**[]Label**](Label.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5ReposOwnerRepoPullsNumberMerge**
> GetV5ReposOwnerRepoPullsNumberMerge(ctx, owner, repo, number, optional)
判断Pull Request是否已经合并

判断Pull Request是否已经合并

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| 仓库所属空间地址(企业、组织或个人的地址path) | 
  **repo** | **string**| 仓库路径(path) | 
  **number** | **int32**| 第几个PR，即本仓库PR的序数 | 
 **optional** | ***PullRequestsApiGetV5ReposOwnerRepoPullsNumberMergeOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a PullRequestsApiGetV5ReposOwnerRepoPullsNumberMergeOpts struct
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **accessToken** | **optional.String**| 用户授权码 | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5ReposOwnerRepoPullsNumberOperateLogs**
> OperateLog GetV5ReposOwnerRepoPullsNumberOperateLogs(ctx, owner, repo, number, optional)
获取某个Pull Request的操作日志

获取某个Pull Request的操作日志

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| 仓库所属空间地址(企业、组织或个人的地址path) | 
  **repo** | **string**| 仓库路径(path) | 
  **number** | **int32**| 第几个PR，即本仓库PR的序数 | 
 **optional** | ***PullRequestsApiGetV5ReposOwnerRepoPullsNumberOperateLogsOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a PullRequestsApiGetV5ReposOwnerRepoPullsNumberOperateLogsOpts struct
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **accessToken** | **optional.String**| 用户授权码 | 
 **sort** | **optional.String**| 按递增(asc)或递减(desc)排序，默认：递减 | [default to desc]

### Return type

[**OperateLog**](OperateLog.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PatchV5ReposOwnerRepoPullsCommentsId**
> PullRequestComments PatchV5ReposOwnerRepoPullsCommentsId(ctx, accessToken, body, owner, repo, id)
编辑评论

编辑评论

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **accessToken** | **string**|  | 
  **body** | **string**|  | 
  **owner** | **string**| 仓库所属空间地址(企业、组织或个人的地址path) | 
  **repo** | **string**| 仓库路径(path) | 
  **id** | **int32**| 评论的ID | 

### Return type

[**PullRequestComments**](PullRequestComments.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PatchV5ReposOwnerRepoPullsNumber**
> PullRequest PatchV5ReposOwnerRepoPullsNumber(ctx, owner, repo, number, optional)
更新Pull Request信息

更新Pull Request信息

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| 仓库所属空间地址(企业、组织或个人的地址path) | 
  **repo** | **string**| 仓库路径(path) | 
  **number** | **int32**| 第几个PR，即本仓库PR的序数 | 
 **optional** | ***PullRequestsApiPatchV5ReposOwnerRepoPullsNumberOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a PullRequestsApiPatchV5ReposOwnerRepoPullsNumberOpts struct
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **accessToken** | **optional.**|  | 
 **title** | **optional.**|  | 
 **body** | **optional.**|  | 
 **state** | **optional.**|  | 
 **milestoneNumber** | **optional.**|  | 
 **labels** | **optional.**|  | 
 **assigneesNumber** | **optional.**|  | 
 **testersNumber** | **optional.**|  | 

### Return type

[**PullRequest**](PullRequest.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PatchV5ReposOwnerRepoPullsNumberAssignees**
> PullRequest PatchV5ReposOwnerRepoPullsNumberAssignees(ctx, owner, repo, number, optional)
重置 Pull Request 审查 的状态

重置 Pull Request 审查 的状态

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| 仓库所属空间地址(企业、组织或个人的地址path) | 
  **repo** | **string**| 仓库路径(path) | 
  **number** | **int32**| 第几个PR，即本仓库PR的序数 | 
 **optional** | ***PullRequestsApiPatchV5ReposOwnerRepoPullsNumberAssigneesOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a PullRequestsApiPatchV5ReposOwnerRepoPullsNumberAssigneesOpts struct
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **accessToken** | **optional.**|  | 
 **resetAll** | **optional.**|  | 

### Return type

[**PullRequest**](PullRequest.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PatchV5ReposOwnerRepoPullsNumberTesters**
> PullRequest PatchV5ReposOwnerRepoPullsNumberTesters(ctx, owner, repo, number, optional)
重置 Pull Request 测试 的状态

重置 Pull Request 测试 的状态

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| 仓库所属空间地址(企业、组织或个人的地址path) | 
  **repo** | **string**| 仓库路径(path) | 
  **number** | **int32**| 第几个PR，即本仓库PR的序数 | 
 **optional** | ***PullRequestsApiPatchV5ReposOwnerRepoPullsNumberTestersOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a PullRequestsApiPatchV5ReposOwnerRepoPullsNumberTestersOpts struct
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **accessToken** | **optional.**|  | 
 **resetAll** | **optional.**|  | 

### Return type

[**PullRequest**](PullRequest.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PostV5ReposOwnerRepoPulls**
> PullRequest PostV5ReposOwnerRepoPulls(ctx, body, owner, repo)
创建Pull Request

创建Pull Request

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **body** | [**RepoPullsBody**](RepoPullsBody.md)|  | 
  **owner** | **string**| 仓库所属空间地址(企业、组织或个人的地址path) | 
  **repo** | **string**| 仓库路径(path) | 

### Return type

[**PullRequest**](PullRequest.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PostV5ReposOwnerRepoPullsNumberAssignees**
> PullRequest PostV5ReposOwnerRepoPullsNumberAssignees(ctx, body, owner, repo, number)
指派用户审查 Pull Request

指派用户审查 Pull Request

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **body** | [**NumberAssigneesBody**](NumberAssigneesBody.md)|  | 
  **owner** | **string**| 仓库所属空间地址(企业、组织或个人的地址path) | 
  **repo** | **string**| 仓库路径(path) | 
  **number** | **int32**| 第几个PR，即本仓库PR的序数 | 

### Return type

[**PullRequest**](PullRequest.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PostV5ReposOwnerRepoPullsNumberComments**
> PullRequestComments PostV5ReposOwnerRepoPullsNumberComments(ctx, body, owner, repo, number)
提交Pull Request评论

提交Pull Request评论

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **body** | [**NumberCommentsBody1**](NumberCommentsBody1.md)|  | 
  **owner** | **string**| 仓库所属空间地址(企业、组织或个人的地址path) | 
  **repo** | **string**| 仓库路径(path) | 
  **number** | **int32**| 第几个PR，即本仓库PR的序数 | 

### Return type

[**PullRequestComments**](PullRequestComments.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PostV5ReposOwnerRepoPullsNumberLabels**
> Label PostV5ReposOwnerRepoPullsNumberLabels(ctx, owner, repo, number, optional)
创建 Pull Request 标签

创建 Pull Request 标签  需要在请求的 body 里填上数组，元素为标签的名字。如: [\"performance\", \"bug\"]

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| 仓库所属空间地址(企业、组织或个人的地址path) | 
  **repo** | **string**| 仓库路径(path) | 
  **number** | **int32**| 第几个PR，即本仓库PR的序数 | 
 **optional** | ***PullRequestsApiPostV5ReposOwnerRepoPullsNumberLabelsOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a PullRequestsApiPostV5ReposOwnerRepoPullsNumberLabelsOpts struct
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **body** | [**optional.Interface of NumberLabelsBody3**](NumberLabelsBody3.md)|  | 

### Return type

[**Label**](Label.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PostV5ReposOwnerRepoPullsNumberReview**
> PostV5ReposOwnerRepoPullsNumberReview(ctx, owner, repo, number, optional)
处理 Pull Request 审查

处理 Pull Request 审查

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| 仓库所属空间地址(企业、组织或个人的地址path) | 
  **repo** | **string**| 仓库路径(path) | 
  **number** | **int32**| 第几个PR，即本仓库PR的序数 | 
 **optional** | ***PullRequestsApiPostV5ReposOwnerRepoPullsNumberReviewOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a PullRequestsApiPostV5ReposOwnerRepoPullsNumberReviewOpts struct
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **body** | [**optional.Interface of NumberReviewBody**](NumberReviewBody.md)|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PostV5ReposOwnerRepoPullsNumberTest**
> PostV5ReposOwnerRepoPullsNumberTest(ctx, owner, repo, number, optional)
处理 Pull Request 测试

处理 Pull Request 测试

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| 仓库所属空间地址(企业、组织或个人的地址path) | 
  **repo** | **string**| 仓库路径(path) | 
  **number** | **int32**| 第几个PR，即本仓库PR的序数 | 
 **optional** | ***PullRequestsApiPostV5ReposOwnerRepoPullsNumberTestOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a PullRequestsApiPostV5ReposOwnerRepoPullsNumberTestOpts struct
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **body** | [**optional.Interface of NumberTestBody**](NumberTestBody.md)|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PostV5ReposOwnerRepoPullsNumberTesters**
> PullRequest PostV5ReposOwnerRepoPullsNumberTesters(ctx, body, owner, repo, number)
指派用户测试 Pull Request

指派用户测试 Pull Request

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **body** | [**NumberTestersBody**](NumberTestersBody.md)|  | 
  **owner** | **string**| 仓库所属空间地址(企业、组织或个人的地址path) | 
  **repo** | **string**| 仓库路径(path) | 
  **number** | **int32**| 第几个PR，即本仓库PR的序数 | 

### Return type

[**PullRequest**](PullRequest.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PutV5ReposOwnerRepoPullsNumberLabels**
> Label PutV5ReposOwnerRepoPullsNumberLabels(ctx, owner, repo, number, optional)
替换 Pull Request 所有标签

替换 Pull Request 所有标签  需要在请求的body里填上数组，元素为标签的名字。如: [\"performance\", \"bug\"]

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| 仓库所属空间地址(企业、组织或个人的地址path) | 
  **repo** | **string**| 仓库路径(path) | 
  **number** | **int32**| 第几个PR，即本仓库PR的序数 | 
 **optional** | ***PullRequestsApiPutV5ReposOwnerRepoPullsNumberLabelsOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a PullRequestsApiPutV5ReposOwnerRepoPullsNumberLabelsOpts struct
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **body** | [**optional.Interface of NumberLabelsBody2**](NumberLabelsBody2.md)|  | 

### Return type

[**Label**](Label.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PutV5ReposOwnerRepoPullsNumberMerge**
> PutV5ReposOwnerRepoPullsNumberMerge(ctx, owner, repo, number, optional)
合并Pull Request

合并Pull Request

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| 仓库所属空间地址(企业、组织或个人的地址path) | 
  **repo** | **string**| 仓库路径(path) | 
  **number** | **int32**| 第几个PR，即本仓库PR的序数 | 
 **optional** | ***PullRequestsApiPutV5ReposOwnerRepoPullsNumberMergeOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a PullRequestsApiPutV5ReposOwnerRepoPullsNumberMergeOpts struct
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **body** | [**optional.Interface of NumberMergeBody**](NumberMergeBody.md)|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


# RepoHooksBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AccessToken** | **string** | 用户授权码 | [optional] [default to null]
**Url** | **string** | 远程HTTP URL | [default to null]
**EncryptionType** | **int32** | 加密类型: 0: 密码, 1: 签名密钥 | [optional] [default to null]
**Password** | **string** | 请求URL时会带上该密码，防止URL被恶意请求 | [optional] [default to null]
**PushEvents** | **bool** | Push代码到仓库 | [optional] [default to true]
**TagPushEvents** | **bool** | 提交Tag到仓库 | [optional] [default to null]
**IssuesEvents** | **bool** | 创建/关闭Issue | [optional] [default to null]
**NoteEvents** | **bool** | 评论了Issue/代码等等 | [optional] [default to null]
**MergeRequestsEvents** | **bool** | 合并请求和合并后 | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


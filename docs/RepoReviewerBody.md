# RepoReviewerBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AccessToken** | **string** | 用户授权码 | [optional] [default to null]
**Assignees** | **string** | 审查人员username，可多个，半角逗号分隔，如：(username1,username2) | [default to null]
**Testers** | **string** | 测试人员username，可多个，半角逗号分隔，如：(username1,username2) | [default to null]
**AssigneesNumber** | **int32** | 最少审查人数 | [default to null]
**TestersNumber** | **int32** | 最少测试人数 | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


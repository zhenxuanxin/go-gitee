# RepoTagsBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AccessToken** | **string** | 用户授权码 | [optional] [default to null]
**Refs** | **string** | 起点名称, 默认：master | [default to master]
**TagName** | **string** | 新创建的标签名称 | [default to null]
**TagMessage** | **string** | Tag 描述, 默认为空 | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


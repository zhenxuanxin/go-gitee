# SettingNewBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AccessToken** | **string** | 用户授权码 | [optional] [default to null]
**Wildcard** | **string** | 分支/通配符 | [default to null]
**Pusher** | **string** | admin: 仓库管理员, none: 禁止任何人合并; 用户: 个人的地址path(多个用户用 &#x27;;&#x27; 隔开) | [default to admin]
**Merger** | **string** | admin: 仓库管理员, none: 禁止任何人合并; 用户: 个人的地址path(多个用户用 &#x27;;&#x27; 隔开) | [default to admin]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


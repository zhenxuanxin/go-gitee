# ShaCommentsBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AccessToken** | **string** | 用户授权码 | [optional] [default to null]
**Body** | **string** | 评论的内容 | [default to null]
**Path** | **string** | 文件的相对路径 | [optional] [default to null]
**Position** | **int32** | Diff的相对行数 | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


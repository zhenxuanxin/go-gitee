/*
 * Gitee Open API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * API version: 5.4.31
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */
package gitee

// 获取代码片段的commit
type CodeForksHistory struct {
	Url string `json:"url,omitempty" xml:"url"`
	ForksUrl string `json:"forks_url,omitempty" xml:"forks_url"`
	CommitsUrl string `json:"commits_url,omitempty" xml:"commits_url"`
	Id string `json:"id,omitempty" xml:"id"`
	Description string `json:"description,omitempty" xml:"description"`
	Public string `json:"public,omitempty" xml:"public"`
	Owner string `json:"owner,omitempty" xml:"owner"`
	User string `json:"user,omitempty" xml:"user"`
	Files string `json:"files,omitempty" xml:"files"`
	Truncated string `json:"truncated,omitempty" xml:"truncated"`
	HtmlUrl string `json:"html_url,omitempty" xml:"html_url"`
	Comments string `json:"comments,omitempty" xml:"comments"`
	CommentsUrl string `json:"comments_url,omitempty" xml:"comments_url"`
	GitPullUrl string `json:"git_pull_url,omitempty" xml:"git_pull_url"`
	GitPushUrl string `json:"git_push_url,omitempty" xml:"git_push_url"`
	CreatedAt string `json:"created_at,omitempty" xml:"created_at"`
	UpdatedAt string `json:"updated_at,omitempty" xml:"updated_at"`
	Forks string `json:"forks,omitempty" xml:"forks"`
	History string `json:"history,omitempty" xml:"history"`
}

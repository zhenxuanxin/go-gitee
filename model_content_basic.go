/*
 * Gitee Open API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * API version: 5.4.31
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */
package gitee

type ContentBasic struct {
	Name string `json:"name,omitempty" xml:"name"`
	Path string `json:"path,omitempty" xml:"path"`
	Size string `json:"size,omitempty" xml:"size"`
	Sha string `json:"sha,omitempty" xml:"sha"`
	Type_ string `json:"type,omitempty" xml:"type"`
	Url string `json:"url,omitempty" xml:"url"`
	HtmlUrl string `json:"html_url,omitempty" xml:"html_url"`
	DownloadUrl string `json:"download_url,omitempty" xml:"download_url"`
	Links string `json:"_links,omitempty" xml:"_links"`
}

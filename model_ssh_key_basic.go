/*
 * Gitee Open API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * API version: 5.4.31
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */
package gitee

// 列出指定用户的所有公钥
type SshKeyBasic struct {
	Id string `json:"id,omitempty" xml:"id"`
	Key string `json:"key,omitempty" xml:"key"`
}

/*
 * Gitee Open API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * API version: 5.4.31
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */
package gitee

// 获取授权用户的资料
type UserDetail struct {
	Id int32 `json:"id,omitempty" xml:"id"`
	Login string `json:"login,omitempty" xml:"login"`
	Name string `json:"name,omitempty" xml:"name"`
	AvatarUrl string `json:"avatar_url,omitempty" xml:"avatar_url"`
	Url string `json:"url,omitempty" xml:"url"`
	HtmlUrl string `json:"html_url,omitempty" xml:"html_url"`
	// 企业备注名
	Remark string `json:"remark,omitempty" xml:"remark"`
	FollowersUrl string `json:"followers_url,omitempty" xml:"followers_url"`
	FollowingUrl string `json:"following_url,omitempty" xml:"following_url"`
	GistsUrl string `json:"gists_url,omitempty" xml:"gists_url"`
	StarredUrl string `json:"starred_url,omitempty" xml:"starred_url"`
	SubscriptionsUrl string `json:"subscriptions_url,omitempty" xml:"subscriptions_url"`
	OrganizationsUrl string `json:"organizations_url,omitempty" xml:"organizations_url"`
	ReposUrl string `json:"repos_url,omitempty" xml:"repos_url"`
	EventsUrl string `json:"events_url,omitempty" xml:"events_url"`
	ReceivedEventsUrl string `json:"received_events_url,omitempty" xml:"received_events_url"`
	Type_ string `json:"type,omitempty" xml:"type"`
	MemberRole string `json:"member_role,omitempty" xml:"member_role"`
	Blog string `json:"blog,omitempty" xml:"blog"`
	Weibo string `json:"weibo,omitempty" xml:"weibo"`
	Bio string `json:"bio,omitempty" xml:"bio"`
	PublicRepos string `json:"public_repos,omitempty" xml:"public_repos"`
	PublicGists string `json:"public_gists,omitempty" xml:"public_gists"`
	Followers string `json:"followers,omitempty" xml:"followers"`
	Following string `json:"following,omitempty" xml:"following"`
	Stared string `json:"stared,omitempty" xml:"stared"`
	Watched string `json:"watched,omitempty" xml:"watched"`
	CreatedAt string `json:"created_at,omitempty" xml:"created_at"`
	UpdatedAt string `json:"updated_at,omitempty" xml:"updated_at"`
	Email string `json:"email,omitempty" xml:"email"`
}

/*
 * Gitee Open API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * API version: 5.4.31
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */
package gitee

// 获取一条私信
type UserMessage struct {
	Id int32 `json:"id,omitempty" xml:"id"`
	Sender *UserBasic `json:"sender,omitempty" xml:"sender"`
	Unread string `json:"unread,omitempty" xml:"unread"`
	Content string `json:"content,omitempty" xml:"content"`
	UpdatedAt string `json:"updated_at,omitempty" xml:"updated_at"`
	Url string `json:"url,omitempty" xml:"url"`
	HtmlUrl string `json:"html_url,omitempty" xml:"html_url"`
}

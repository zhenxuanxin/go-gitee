#!/bin/bash

current_dir=$(dirname $0)
base_dir=$(dirname ${current_dir})

# generate from server
#swagger-codegen generate -l go -c ${base_dir}/configs/config.json -i https://gitee.com/api/v5/doc_json -o ${base_dir}

# generate from local
swagger-codegen generate -l go -c ${base_dir}/configs/config.json -i ${base_dir}/api/swagger.json -o ${base_dir}
